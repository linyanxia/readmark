class Array(object):
    """
    创建数组类并调用:实现根据下标访问数组元素、根据下标重设数组元素、查看数组长度、清空数组、数组历遍等功能
    """

    def __init__(self, size=10):
        """
        :param size: 数组长度
        """
        self._size = size
        self._items = [None] * size

    def __getitem__(self, item):
        """
        :param item: 下标
        :return:根据下标访问数组元素
        """
        return self._items[item]

    def __setitem__(self, key, value):
        """
        :param key: 下标
        :param value:重设值
        :return:根据下标重设数组元素
        """
        self._items[key] = value

    def __len__(self):
        """
        :return: 查看数组长度
        """
        return self._size

    def clear(self, value=None):
        """
        :param value: None
        :return: 元素全为空的数组
        """
        for i in range(len(self._items)):
            self._items[i] = value

    def __iter__(self):
        """
        :return: 数组历遍结果
        """
        for item in self._items:
            yield item


class Node(object):
    """
    定义链表节点
    """

    def __init__(self, value=None, next=None):
        self.value, self.next = value, next


class Linked_list(object):
    """
    创建链表：实现链表类插入、删除等功能
    """

    def __init__(self, size=None):
        """
        :param size:链表长度
        """
        self.size = size
        self.root = Node()
        self.tail_node = None
        self.length = 0

    def __len__(self):
        return self.length

    def append(self, vaule):
        if self.size and len(self) >= self.size:
            raise Exception("Linked_list IS FUL.")
        node = Node(value)
        tail_node = self.tail_node


if __name__ == '__main__':
    a = Array(5)
    print(a.__len__())
    print(a[4])
    a[4] = 100
    print(a[4])
    for i in a:
        print('i:\n', i)
    a.clear()
    print("清空数组后")
    for i in a:
        print('i:\n', i)
