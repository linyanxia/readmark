# 1、复杂度分析：包括时间、空间复杂度分析
# 区别于事后统计法（运行后统计、监控得到算法的执行时间和占用内存），避免测试环境、数据规模等其他因素影响
# 基本假设：
# (1)每行代码执行时间为unit_time
# (2)n为数据规模
# (3)所有代码执行的时间为T(n)
# 大O表示法：T()=O(f(n))
# 分析理论归纳：
# (1)只关注循环执行次数最多的部分代码
# (2)加法法则：总复杂度等于量级最大的部分代码的复杂度
# (3)乘法法则：嵌套代码的复杂度等于嵌套内外代码复杂度的乘积
# 复杂度量级：
# (1)多项式量级：常量阶 O(1)、对数阶 O(logn)、线性阶 O(n)、线性对数阶 O(nlogn)、平方阶 O(n**2)、立方阶O(n**3)
# (2)非多项式量级：指数阶 O(2**n)、阶乘阶 O(n!)
# 最好、最坏、平均、均摊时间复杂度

# 例子解析1:
def example_test1(n):
    sum=0
    for i in range(1,n+1):
        sum = sum +i
    return sum

# T(n)=（1+n+n）*unit_time   <==>  T(n)=O(2n+1)    <==>    T(n)=O(n)

# 例子解析2:
def example_test2(n):
    sum=0
    for i in range(1,n+1):
        for j in range(1,n+1):        
            sum = sum +i*j
    return sum

# T(n)=（1+n**2+n**2+n**2）*unit_time   <==>  T(n)=O(3*n**2+1)    <==>    T(n)=O(n**2)


# 例子解析3:
def example_test3(n):
    sum_1,sum_2,sum_3=0,0,0
    for i in range(100):
        sum_1+=i
    for j in range(n):
        sum_2+=j
    for k in range(n):
        for m in range(n):
            sum_3+=k*m
    return sum_1+sum_2+sum_3

# T(n)=T1(n)+T2(n)=O(max(O(f(n),g(n))))  <==>  T(n)=O(n**2)

# 例子解析4:
def example_test4(n):
    def sum_test(m):
        sum=0
        for i in range(1,n+1):
            sum+=i
        return sum
    ret=0
    for j in range(1,n+1):
        ret+=sum_test(j)
    return ret

# T(n)=T1(n)*T2(n)=O(n*n)  <==>  T(n)=O(n**2)


# 例子解析5:
def example_test5(n):
    sum_1,sum_2=0
    for i in range(1,n+1):
        sum_1 +=i
    for j in range(1,m+1):
        sum_2 +=j   
    return sum
# T(n+m)=T1(n)+T2(m) <==>  T(n)=O(n+m)
