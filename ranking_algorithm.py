#排序算法使用场景：

#1，空间复杂度 越低越好、n值较大：

#　　堆排序　　O(nlog2n)　　O(1)

#2，无空间复杂度要求、n值较大：

#　　桶排序　　O(n+k)　　　　O(n+k)



# 冒泡排序
def bubbleSort(input_list):
    """
    算法思路：它重复地走访要排序的数列，一次比较两个元素，如果他们的顺序错误就把他们交换过来。走访数列的工作是重复地进行直到没有再需要交换，也就是说该数列已经排序完成
    平均时间复杂度：平方阶 O(n**2)
    稳定性：稳定
    复杂性：简单
    :param input_list:
    :return:sorted_list
    """
    if len(input_list) <= 0:
        return input_list
    for i in range(len(input_list)):
        for j in range(0, len(input_list) - 1):
            if input_list[j + 1] < input_list[j]:
                input_list[j], input_list[j + 1] = input_list[j + 1], input_list[j]
            j += 1
    return input_list

# def bubblesort(input_list):
#     if len(input_list) == 0:
#         return input_list
#     for i in range(len(input_list)):
#         flag = True
#         for j in range(0, len(input_list) - 1):
#             if input_list[j] > input_list[j + 1]:
#                 input_list[j], input_list[j + 1] = input_list[j + 1], input_list[j]
#                 flag = False
#         if flag:
#             break
#     return input_list


# 快速排序

def QuickSort(input_list, i, j):
    """
    算法思路：快速排序的基本思想是：通过一趟排序将要排序的数据分割成独立的两部分：分割点左边都是比它小的数，右边都是比它大的数
    平均时间复杂度：线性对数阶 O(nlogn)
    稳定性：不稳定
    复杂性：较复杂
    :param input_list:
    :param i:
    :param j:
    :return:
    """
    def partion(input_list, i, j):
        base = input_list[i]
        start = i
        end = j
        while j > i:
            while j > i and input_list[j] >= base:
                j -= 1
            temp1 = input_list[j]
            while j > i and input_list[i] <= base:
                i += 1
            temp2 = input_list[i]
            input_list[i], input_list[j] = temp1, temp2
        if i >= j:
            input_list[start] = input_list[i]
            input_list[i] = base
        return i

    if i < j:
        index = partion(input_list, i, j)
        QuickSort(input_list, i, index - 1)  # 使用递归函数原理，利用分区index重新计算
        QuickSort(input_list, index + 1, j)
    return input_list


# def quicksort(input_list, i, j):
#     def partionq(input_list, i, j):
#         if len(input_list) <= 1:
#             return input_list
#         base = input_list[i]
#         b_index = i
#         while i < j:
#             while j > i and input_list[j] >= base:
#                 j = j - 1
#             temp1 = input_list[j]
#             while i < j and input_list[i] <= base:
#                 i += 1
#             temp2 = input_list[i]
#             input_list[i], input_list[j] = input_list[j], input_list[i]
#         if i >= j:
#             input_list[b_index] = input_list[i]
#             input_list[i] = base
#         return i
#
#     if i < j:
#         index = partionq(input_list, i, j)
#         quicksort(input_list, i, index - 1)
#         quicksort(input_list, index + 1, j)
#     return input_list


# 插入排序
def insertSort(input_list):
    """
    算法思路：每一趟将一个待排序的记录，按照其关键字的大小插入到有序队列的合适位置里，知道全部插入完成。
    平均时间复杂度：平方阶 O(n**2)
    稳定性：稳定
    复杂性：简单
    :param :input_list
    :return:sorted_list
    """
    for i in range(1, len(input_list)):
        j = i - 1
        temp = input_list[i]
        while j >= 0 and temp < input_list[j]:
            input_list[j + 1] = input_list[j]
            j -= 1
        input_list[j + 1] = temp
    return input_list


# def insertsort(input_list):
#     if len(input_list) == 0:
#         return input_list
#     for i in range(1, len(input_list)):
#         temp = input_list[i]
#         j = i - 1
#         while j >= 0 and temp < input_list[j]:
#             input_list[j + 1] = input_list[j]
#             j = j - 1
#         input_list[j + 1] = temp
#     return input_list


# 希尔排序
def shellSort(input_list):
    """
    算法思路：步长的选择是希尔排序的重要部分。算法最开始以一定的步长进行排序，然后会继续以更小的步长进行排序，最终算法以步长为 1 进行排序。当步长为 1 时，算法变为直接插入排序，这就保证了数据一定会被全部排序。
    平均时间复杂度：线性对数阶 O(nlogn)
    稳定性：不稳定
    复杂性：较复杂
    :param input_list:
    :return: sorted_list
    """
    if len(input_list) <= 0:
        return input_list
    length = len(input_list)
    gap = length // 2
    while gap >= 1:
        for i in range(gap, len(input_list)):
            j = i - gap
            for j in range(0, len(input_list) - gap):
                if input_list[j] > input_list[j + gap]:
                    input_list[j], input_list[j + gap] = input_list[j + gap], input_list[j]
        gap = gap // 2
    return input_list


# 选择排序
def selectSort(input_list):
    """
    算法思路：简单排序很简单，它的大致处理流程为：1、从待排序序列中，找到关键字最小的元素；2、如果最小元素不是待排序序列的第一个元素，将其和第一个元素互换；3、从余下的 N - 1 个元素中，找出关键字最小的元素，重复(1)、(2)步，直到排序结束。
    平均时间复杂度：平方阶 O(n**2)
    稳定性：不稳定
    复杂性：简单
    :param input_list:
    :return:sorted_list
    """
    if len(input_list) <= 0:
        return input_list
    for i in range(len(input_list) - 1):
        min_index = i
        j = i + 1
        while j <= (len(input_list) - 1):
            if input_list[min_index] > input_list[j]:
                min_index = j
            j = j + 1
        temp = input_list[i]
        input_list[i] = input_list[min_index]
        input_list[min_index] = temp
    return input_list


def selectsort(input_list):
    for i in range(len(input_list)):
        min_index = i
        temp = input_list[i]
        j = i + 1
        while j <= len(input_list) - 1:
            if input_list[min_index] > input_list[j]:
                min_index = j
            j += 1
        input_list[i] = input_list[min_index]
        input_list[min_index] = temp
    return input_list


# 堆排序
def HeadSort(input_list):
    """
    算法思路：选择排序：每趟从待排序的记录中选出关键字最小的记录，顺序放在已排序的记录序列末尾，直到全部排序结束为止
    平均时间复杂度：线性对数阶 O(nlogn)
    稳定性：不稳定
    复杂性：较复杂
    :param input_list:
    :return: sorted_list
    """
    def HeadAdjust(input_list, parent, length):
        if len(input_list) == 0:
            return []
        temp = input_list[parent]
        child = 2 * parent + 1
        while child < length:
            if child + 1 < length and input_list[child] < input_list[child + 1]:
                child += 1
            if temp >= input_list[child]:
                break
            input_list[parent] = input_list[child]
            parent = child
            child = 2 * parent + 1
        input_list[parent] = temp

    sorted_list = input_list
    length = len(sorted_list)
    for i in range(0, length // 2)[::-1]:
        HeadAdjust(sorted_list, i, length)
    for j in range(1, length)[::-1]:
        temp = sorted_list[j]
        sorted_list[j] = sorted_list[0]
        sorted_list[0] = temp
        HeadAdjust(sorted_list, 0, j)
    return sorted_list


# 归并排序
def MergeSort(input_list):
    """
    算法思路：该算法采用经典的分治（divide-and-conquer）策略（分治法将问题分(divide)成一些小的问题然后递归求解，而治(conquer)的阶段则将分的阶段得到的各答案"修补"在一起，即分而治之)
    平均时间复杂度：线性对数阶 O(nlog2n)
    稳定性：稳定
    复杂性：较复杂
    :param input_list:
    :return:sorted_list
    """
    def merge(input_list, left, mid, right, temp):
        '''
        函数说明:合并函数
        Author:
            www.cuijiahua.com
        Parameters:
            input_list - 待合并列表
            left - 左指针
            right - 右指针
            temp - 临时列表
        Returns:
            无
        '''
        i = left
        j = mid + 1
        k = 0
        while i <= mid and j <= right:
            if input_list[i] <= input_list[j]:
                temp[k] = input_list[i]
                i += 1
            else:
                temp[k] = input_list[j]
                j += 1
            k += 1
        while i <= mid:
            temp[k] = input_list[i]
            i += 1
            k += 1
        while j <= right:
            temp[k] = input_list[j]
            j += 1
            k += 1
        k = 0
        while left <= right:
            input_list[left] = temp[k]
            left += 1
            k += 1

    def merge_sort(input_list, left, right, temp):
        if left >= right:
            return
        mid = (right + left) // 2
        merge_sort(input_list, left, mid, temp)
        merge_sort(input_list, mid + 1, right, temp)

        merge(input_list, left, mid, right, temp)

    if len(input_list) == 0:
        return []
    sorted_list = input_list
    temp = [0] * len(sorted_list)
    merge_sort(sorted_list, 0, len(sorted_list) - 1, temp)
    return sorted_list


# 基数排序
def RadixSort(input_list):
    '''
    算法思路：基本思想：将所有待比较数值（正整数）统一为同样的数位长度，数位较短的数前面补零。然后，从最低位开始，依次进行一次排序。这样从最低位排序一直到最高位排序完成以后,数列就变成一个有序序列。
    平均时间复杂度：线性阶 O(d*n)
    稳定性：稳定
    复杂性：较复杂
    Parameters:input_list - 待排序列表
    Returns:sorted_list - 升序排序好的列表
    '''

    def MaxBit(input_list):
        '''
        函数说明:求出数组中最大数的位数的函数
        Author:
            www.cuijiahua.com
        Parameters:
            input_list - 待排序列表
        Returns:
            bits-num - 位数
        '''
        max_data = max(input_list)
        bits_num = 0
        while max_data:
            bits_num += 1
            max_data //= 10
        return bits_num

    def digit(num, d):
        '''
        函数说明:取数xxx上的第d位数字
        Author:
            www.cuijiahua.com
        Parameters:
            num - 待操作的数
            d - 第d位的数
        Returns:
            取数结果
        '''
        p = 1
        while d > 1:
            d -= 1
            p *= 10
        return num // p % 10

    if len(input_list) == 0:
        return []
    sorted_list = input_list
    length = len(sorted_list)
    bucket = [0] * length

    for d in range(1, MaxBit(sorted_list) + 1):
        count = [0] * 10

        for i in range(0, length):
            count[digit(sorted_list[i], d)] += 1

        for i in range(1, 10):
            count[i] += count[i - 1]

        for i in range(0, length)[::-1]:
            k = digit(sorted_list[i], d)
            bucket[count[k] - 1] = sorted_list[i]
            count[k] -= 1
        for i in range(0, length):
            sorted_list[i] = bucket[i]

    return sorted_list


if __name__ == '__main__':
    input_list = [5, 3, 1, 7, 0, 0]
    print('排序前', input_list)
    sort_list = bubbleSort(input_list)
    print('排序后', sort_list)
    input_list = [9, 3, 1, 7, 9, 0]
    print('排序前', input_list)
    sort_list = bubblesort(input_list)
    print('排序后', sort_list)
    input_list = [6, 1, 2, 7, 9, 3, 4, 5, 10, 10]
    print('排序前', input_list)
    sort_list = QuickSort(input_list, 0, len(input_list) - 1)
    print('排序后', sort_list)
    input_list = [6, 1, 2, 7, 9, 3, 4, 5, 10, 10]
    print('排序前', input_list)
    sort_list = quicksort(input_list, 0, len(input_list) - 1)
    print('排序后', sort_list)
    input_list = [5, 3, 1, 7, 0, 9]
    print('排序前', input_list)
    sort_list = insertSort(input_list)
    print('排序后', sort_list)
    input_list = [5, 3, 1, 7, 9, 9]
    print('排序前', input_list)
    sort_list = insertsort(input_list)
    print('排序后', sort_list)
    input_list = [5, 3, 1, 7, 9, 10, 0]
    print('排序前', input_list)
    sort_list = shellSort(input_list)
    print('排序后', sort_list)
    input_list = [5, 3, 1, 7, 0]
    print('排序前', input_list)
    sort_list = selectSort(input_list)
    print('排序后', sort_list)
    input_list = [0, 5, 3, 0, 1, 7, 9]
    print('排序前', input_list)
    sort_list = selectsort(input_list)
    print('排序后', sort_list)
    input_list = [6, 4, 8, 9, 2, 3, 1]
    print('排序前:', input_list)
    sorted_list = HeadSort(input_list)
    print('排序后:', sorted_list)
    input_list = [6, 4, 8, 9, 2, 3, 1]
    print('排序前:', input_list)
    sorted_list = MergeSort(input_list)
    print('排序后:', sorted_list)
    input_list = [50, 123, 543, 187, 49, 30, 0, 2, 11, 100]
    print('排序前:', input_list)
    sorted_list = RadixSort(input_list)
    print('排序后:', sorted_list)
